/********************************************************
	Copie de fichier
*********************************************************/


#define _GNU_SOURCE

#include <wiringPi.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <time.h>

#include <unistd.h> /* close */
#include <netdb.h> /* gethostbyname */

#include <pthread.h>

#include <sched.h>


/******************
Variables globales 
*******************/
#define NB_ACQ 500
//Parametres thread
struct argument {
	int a;
	long long int ret[NB_ACQ];
};
//Priorité des thread pour le scheduler
int prio=99;

/************************
Prototypes des fonctions 
************************/

long long int get_my_nanotime();
void *Thread_Lect(void *pin); 


/**********
*** MAIN **
***********/

int main (int argc, char *argv[]){

	FILE *fin = fopen("test.txt","r");
	FILE *fout = fopen("tmp.txt","w");;

	int ch;
	int a =0;
	const char * b= " ";

	for(;;){
		ch=fgetc(fin);
		if(ch == '\n' || ch == EOF) {
			//fputs(b,fout);
			fprintf(fout,"%d",a);
			if (ch == EOF) {
				break;
			}
			a++;
		}
		fputc(ch,fout);

	}

}

