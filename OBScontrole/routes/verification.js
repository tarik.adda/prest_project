//Route dédié à la communication et le fonctionnement de l'OBS

var serial = require('serialport');
var express = require('express');
var router = express.Router();
const { execFile } = require('child_process');
var modV = require('../models/historique.js');
var spawn = require("child_process").spawn;
var path = require('./index.js');
var bodyParser = require('body-parser');

const { StringDecoder } = require('string_decoder');
const decoder = new StringDecoder('utf-8');

const Readline = require('@serialport/parser-readline');

var compo;

var parsi;

var actif={};
//var freq=[[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]];

var value_adc={
	strap:[512,819,1024,1638],
	freq:[[500,250,125,62.5,31.25],[800,400,200,100,50],[1000,500,250,125,62.5],[1600,800,400,200,100]],
	gain:[1,2,4,16,32,64]
}

var tmp="";
var flag=0;
var child;

var tampon_verif = "Under sous la construction";

var session;

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

var dataInfo ={};

let filtered_keys = (obj, filter) => {
  let key, keys = []
  for (key in obj)
    if (obj.hasOwnProperty(key) && filter.test(key))
      keys.push(key)
  return keys
}

router.get('/',function(req,res){
	console.log("Verification");
	session = req.sessionID;
	console.log(path.path);

	res.render('verification',{data:value_adc});
});

router.post('/',function(req,res){
	let temp = {};
	console.log("Verification_fct");
	session = req.sessionID;
	console.log(path.path);
	console.log(req.body);
	console.log(typeof(req.body));



	let p=[]
	let arr = Object.keys(req.body).filter(function(q){return /channeli/gi.test(q)});
	arr.forEach(element=>{
		console.log(element);
		p.push(value_adc.gain[req.body[element]])
	});
	console.log("ouverture de la connection série");
	console.log(p);



	temp.channel = parseInt(req.body.channels)+1;
	temp.description = req.body.description;
	temp.strap = value_adc.strap[req.body.strap];
	temp.echantillon = value_adc.freq[req.body.strap][req.body.echantillon];
	temp.type = req.body.type;
	temp.channeli = p;
	temp.compress = req.body.compress || "off";


	console.log(temp);

//	console.log(Object.keys(req.body).find(function(q){return /channeli/gi.test(req.body)}));
//	let filteredNames = filtered_keys(req.body,/channeli/)


///////////////
//PARAMETRAGE//

	compo = new serial(path.path, {baudRate: 9600}, function(err){
		if(err){
			actif.etat=0;
			res.redirect('/');
			console.log("convertisseur serie non branché");
		} else{
			console.log("connection série initialisé");
		}
	});

//	parsi = compo.pipe(new Readline({delimiter: '\n' }));
	var selec=0;

	compo.on('open',async function(){

		while(!selec){
			compo.write('\r');
			await sleep(100);
		}

		console.log("Data a envoyer");
		console.log(temp);
		console.log("param");
		compo.write('p\r')
		await sleep(100);
	 	console.log("non");
		compo.write('n\r')
		await sleep(100);
	 	console.log("description");
		compo.write(temp.description+'p\r')
		await sleep(100);

/*	temp.channel = req.body.channels;
	temp.description = req.body.description;
	temp.strap = req.body.strap;
	temp.echantillon = req.body.echantillon;
	temp.type = req.body.type;
	temp.channeli = p;
	temp.compress = req.body.compress || "off";
*/
	 	console.log("strap");
		compo.write(temp.strap+'\r')
		await sleep(100);
	 	console.log("sample");
		compo.write(temp.echantillon+'\r')
		await sleep(100);
	 	console.log("chan");
		compo.write(temp.channel+'\r')
		await sleep(100);
	 	console.log("gains chan");
		compo.write('p\r')
		await sleep(100);
	 	console.log("end of exp");
		compo.write('p\r')
		await sleep(100);
	 	console.log("compress");
		compo.write('p\r')
		await sleep(3000);
	 	console.log("yes");
		compo.write('y\r')
		await sleep(100);
	 	console.log("fin");
		compo.close();
	});

	compo.on('close',function(){
		console.log("fermeture");
	});

	compo.on('error',function(){
		console.log("erreur");
	});

	compo.on('data',function(data){
		if(data.toString().includes("Sel"){
			selec=1;
		}
		console.log("reception data");
		console.log(data);
	});

//	parsi.on('data',function(data){
//		console.log(data);
//		compo.write('\r')
//		compo.close();
		
	});

/////////////////



	res.render('verification_fct',{data: temp});

/*{channel : temreq.body.channels, 
	description: req.body.description, 
	strap: req.body.strap, 
	echantillon: req.body.echantillon, 
	type: req.body.type, 
	channeli: p,
	compress: req.body.compress}});
*/



});



module.exports = router;
/*
//Fonction gerant la communication direct avec le port serie de l'OBS soit l'écriture
module.exports.func = function(socket){
	socket.on('commande',function(msg){
		console.log('commande '+msg);
		compa.write(msg);
		compa.write('\r');
	});
}
*/

//Fonction démarrant une communication avec l'OBS
module.exports.verif = function(socket){
	socket.on('verif',function(data){
		tampon_verif="Encore en construction";
		switch (data.id) {
			case 'verif_lect_capt' :
		//		socket.emit("retour_verif_lect_capt",tampon_verif);
				console.log('verif lect capt');
				console.log(path.path);

			//	console.log("bien recu bien recu param_time");
//				console.log(path.path);
				compo = new serial(path.path, {baudRate: 9600}, function(err){
					if(err){
						//actif.etat=0;
						//res.redirect('/');
						console.log("convertisseur serie non branché");
					} else{
						console.log("connection série initialisé");
					}
				});
				compo.write('a\r')

				parsi = compo.pipe(new Readline({delimiter: '\n' }));

				compo.on('open',function(){
					console.log("ouverture");
				});

				compo.on('close',function(){
					console.log("fermeture");
				});

				compo.on('error',function(){
					console.log("erreur");
				});

				compo.on('data',function(data){
					console.log(data);
				});

				parsi.on('data',function(data){
					console.log(data);
					compo.close();
				});


			break;
			case 'verif_capt' :
				socket.emit("retour_ver",tampon_verif);
				console.log('verif  capt');
				break;
			case 'verif_record' :
				socket.emit("retour_ver",tampon_verif);
				console.log('verif record');
				break;
			case 'verif_data' :
				socket.emit("retour_ver",tampon_verif);
				console.log('verif data');
				break;
			case 'verif_conso' :
				console.log(data.seuil_b);
				console.log(data.seuil_h);

//DEBUT
				tampon_verif = "";
				console.log("verif_conso lance cote serveur");
				child = spawn('python3', ['/home/pi/TravailPIGPIO/tens_cur/acq.py']);

				child.stdout.on('data',function(data){				
					console.log("data",data);
					tampon_verif = tampon_verif + data;
				});

				child.on('exit',function(data){				
					console.log("fin",tampon_verif);
					socket.emit("retour_ver",tampon_verif);
				});

				//Save dans la BDD
/*
					if (error) {
						//throw error;
						tampon_verif="Erreur lancement du programme de derive d'horloge";
						console.log("erreur a l'execution de la derive");
						modV.createInfo(dataInfo,session,"derive horloge",stdout);
						socket.emit("retour_ver",stdout);
					}
					else { 
						tampon_verif=stdout;
						console.log(typeof(stdout));
						console.log(stdout);
						//Sauvegarde dans la BDD
						modV.createInfo(dataInfo,session,"derive horloge",stdout);
						socket.emit("retour_ver",tampon_verif);

					}
				});
				console.log("emission veri");

*/				break;



//FIN
				console.log('verif conso');
				break;
			case 'verif_time' :
				socket.emit("retour_ver",tampon_verif);
				console.log('verif temps');
				break;
			case 'verif_param' :
				socket.emit("retour_ver",tampon_verif);
//DEBUT SCRIPT


//FIN SCRIPT


				console.log('verif param');
				break;
			case 'param_time' :
				console.log("bien recu bien recu param_time");
				console.log(path.path);
				compo = new serial(path.path, {baudRate: 9600}, function(err){
					if(err){
						actif.etat=0;
						res.redirect('/');
						console.log("convertisseur serie non branché");
					} else{
						console.log("connection série initialisé");
					}
				});
				parsi = compo.pipe(new Readline({delimiter: '\n' }));

				compo.on('open',function(){
					console.log("ouverture");
					compo.write('\r')
				});

				compo.on('close',function(){
					console.log("fermeture");
				});

				compo.on('error',function(){
					console.log("erreur");
				});

				compo.on('data',function(data){
					console.log(data);
				});

				parsi.on('data',function(data){
					console.log(data);
					compo.close();
				});


				//Ouverture du menu synchro
				//Recuperation date et formatage 
				//Envois de la date de synchro
				//Save dans la BDD de la synchro

				socket.emit("retour_ver",tampon_verif);
				console.log('param time');
				break;

			case 'param_acq' :
				socket.emit("retour_ver",tampon_verif);
				console.log('param_acq');
				break;
			case 'info_sys' :
				socket.emit("retour_ver",tampon_verif);
				console.log('info sys');
				break;
			case 'recup_data' :
				socket.emit("retour_ver",tampon_verif);
				console.log('recup_data');
				break;
			case 'verif_derive' :
				console.log("verif_derive lance cote serveur");
				child = execFile('/home/pi/TravailPIGPIO/gpio_test',(error,stdout,stderr) => {
					if (error) {
						//throw error;
						tampon_verif="Erreur lancement du programme de derive d'horloge";
						console.log("erreur a l'execution de la derive");
						modV.createInfo(dataInfo,session,"derive horloge",stdout);
						socket.emit("retour_ver",stdout);
					}
					else { 
						tampon_verif=stdout;
						console.log(typeof(stdout));
						console.log(stdout);
						//Sauvegarde dans la BDD
						modV.createInfo(dataInfo,session,"derive horloge",stdout);
						socket.emit("retour_ver",tampon_verif);
					}
				});
				console.log("emission veri");
				break;
		default :
			console.log('no opt');
			break;

	}


/*
		console.log("Ordre recu: "+id);
		console.log('creation de la liaison serie');
		compo = new serial('/dev/OBS_serial', {baudRate: 9600}, function(err){
			if(err){
				actif.etat=0;
				res.redirect('/');
				console.log("convertisseur serie non branché");
			} else{
				console.log("connection série initialisé");
			}
		});
		console.log("configuration de la liaison série");
		console.log("configuration des events");

		compo.on('close',function(){
			console.log("fermeture");

		});

		compo.on('error',function(){
			console.log("erreur");
		});

		compo.on('data',function(data){
			//Buffering
			tmp+=decoder.write(data);
			//Si une trame entiere est recu
			if(tmp.includes('\r') || tmp.includes('?')) {
				console.log(tmp);
				//On rentre dans le mode select
				if(tmp.includes('Select')) {
					switch (id) {
						case 'verif_lect_capt' :
							console.log('verif lect capt');
							break;
						case 'verif_capt' :
							console.log('verif  capt');
							break;
						case 'verif_record' :
							console.log('verif record');
							break;
						case 'verif_data' :
							console.log('verif data');
							break;
						case 'verif_conso' :
							console.log('verif conso');
							break;
						case 'verif_time' :
							console.log('verif temps');
							break;
						case 'verif_param' :
							console.log('verif param');
							break;
						case 'param_time' :
							console.log('param time');
							break;
						case 'param_acq' :
							console.log('param_acq');
							break;
						case 'info_sys' :
							console.log('info sys');
							break;
						case 'recup_data' :
							console.log('recup_data');
							break;
						default :
							console.log('no opt');
					}
				}
				else {
					compo.write('\r');
				}
		 	}
		});

		compo.on('open',function(){
			console.log("ouverture");
			compo.write('\r')
		});
*/
	});
}

/*
//Fonction permettant de rafraichir l'OBS
module.exports.func2 = function(socket){
	socket.on('refresh',function(){
		console.log('rafraichissement ');
		compa.write('\r');
	});
}
//Fonction permettant de gerer la fermeture de l'OBS
module.exports.func3 = function(socket){
	socket.on('end',function(msg){
		console.log('end');
		compa.close();
	});
}

*/

module.exports.autotest = function(socket){
        socket.on('auto_test',function(data){
                console.log("Procedure d'autotest lancé");

                tampon_test.sys_obs="RIEN NE MARCHE DANS L'OBS";
                tampon_test.sys_capteur="RIEN NE MARCHE DANS LE CAPTEUR";
                tampon_test.sys_adc="RIEN NE MARCHE DANS L'ADC";
                tampon_test.sys_horloge="RIEN NE MARCHE DANS L'HORLOGE";
                tampon_test.sys_memoire="RIEN NE MARCHE DANS LA MEMOIRE";
                tampon_test.sys_conso="RIEN NE MARCHE DANS LA CONSO";

                socket.emit("retour_auto_test",tampon_test);
        });
}


