****************************************************************************
*  Program    :	RTOS
*  Module     :	CONFIG.SRC
*  Unit       :	
*  Purpose    : ROOT SEGMENT FOR CRC (ALLEN NANCE)
*  Host       :	NEC 386/20   MS DOS 3.3   Microtec ASM68K 6.3B 
*  Target     :	RT294   MC68000   RTOS
*  Packages   :	none
*  Author     :	Allen Nance
*****************************************************************************
        TTL	'MC68000 -- CONFIGURE MC68000 SYSTEM'
*
	XREF	.ROOT,BOOT
	XREF	NLINIT,NLOPEN,NLCLOS,NLREAD,NLWRIT,NLCNTL
	XREF	TPINIT,TPOPEN,TPCLOS,TPREAD,TPWRIT,TPCNTL
	XREF	CKINIT,CKOPEN,CKCLOS,CKREAD,CKWRIT,CKCNTL

	XREF	T0_INT,CK_INT,TG_INT,TP_INT,AD_INT
	XREF	FTBINT,T1_INT

	XDEF	RESET2,TRAPADR

TPUBASE	  EQU	$FFFE00
TMCR	  EQU	TPUBASE+0
QMCR      EQU   $FFFFFC00           BCC M68332 QSM MCR Register
*
K	EQU	1024
C_ROOT_STACKSIZE	EQU	4*K
C_ROOT_HEAPSIZE		EQU	0
*
C_NPROC	EQU	4		;MAX # OF PROCESSES
C_NIOD	EQU	4		;MAX # OF I/O DEVICES
C_SYS_STACKSIZE	EQU	4*K	;SYSTEM SUPERVISOR STACK SIZE
C_RAMSTART1	EQU	$0800	;START AFTER INTERRUPT VECTORS
C_RAMEND1	EQU	32*K
C_RAMSTART2	EQU	32*K
C_RAMEND2	EQU	128*K
AVECTRAM	EQU	1024
*       

	INCLUDE	SYSDEF.S
        PAGE
*
*       VECTOR DEFINITIONS FOR  INTERRUPTS
*

        SECTION	syscode
RESET2  DC.L    CONTAB          ;CONFIGURATION TABLE ADDRESS
        DC.L    SYSENT          ;RESET TO BOOT CODE
        DC.L    BUSERR          ;BUS ERROR
        DC.L    ADRERR          ;ADDRESS ERROR
        DC.L    ILLINS          ;ILLEGAL INSTRUCTION
        DC.L    ZERDIV          ;ZERO DIVIDE
        DC.L    CHKINS          ;CHK INSTRUCTION
        DC.L    TRAP_V          ;TRAPV INSTRUCTION
        DC.L    PRVVIL          ;PRIVILEGE VIOLATION
        DC.L    TRACE		;TRACE
        DC.L    LIN101		;LINE 1010 EMULATOR
	DC.L    LIN111		;LINE 1111 EMULATOR
        DC.L    UNUSED          ;RESERVED
        DC.L    UNUSED          ;RESERVED
        DC.L    UNUSED          ;RESERVED
        DC.L    UNINIT		;UNINITIALIZED INTERRUPT VECTOR
        DC.L    UNUSED          ;RESERVED
        DC.L    UNUSED          ;RESERVED
        DC.L    UNUSED          ;RESERVED
        DC.L    UNUSED          ;RESERVED
        DC.L    UNUSED          ;RESERVED
        DC.L    UNUSED          ;RESERVED
        DC.L    UNUSED          ;RESERVED
        DC.L    UNUSED          ;RESERVED
        DC.L    SPRINT		;SPURIOUS INTERRUPT
	DC.L    VCTRAM          ;LEVEL 1 INTERRUPT AUTOVECTOR
        DC.L    VCTRAM          ;LEVEL 2 INTERRUPT AUTOVECTOR
	DC.L    VCTRAM          ;LEVEL 3 INTERRUPT AUTOVECTOR
        DC.L    VCTRAM          ;LEVEL 4 INTERRUPT AUTOVECTOR
        DC.L    VCTRAM          ;LEVEL 5 INTERRUPT AUTOVECTOR
        DC.L    VCTRAM          ;LEVEL 6 INTERRUPT AUTOVECTOR
        DC.L    UNUSED          ;LEVEL 7 INTERRUPT AUTOVECTOR
TRAPADR DC.L    TK$SCH          ;TRAP VECTOR #0 - RESCHEDULE
        DC.L    SERVIC          ;TRAP VECTOR #1 - O.S. SERVICE CALLS
        DC.L    IOSERV          ;TRAP VECTOR #2 - I/O SERVICE
        DC.L    SPVON,SPVOFF,INTON,INTOFF,LPSTOP
        DC.L    UNUSED,UNUSED,UNUSED,UNUSED,UNUSED,UNUSED,UNUSED
	DC.L	GOREST
*
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM
        DC.L    VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM,VCTRAM

;Interrupt Redirection
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT
	DC.L	NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT,NOINIT

        PAGE
BUSERR	MOVE	#1,EXC_ERR	;FLAG EXCEPTION CODE
	MOVE.L	2(SP),EXC_FLT	;STORE FAULT ADDRESS
	MOVE.L	10(SP),EXC_ADR	;STORE ADDRESS
	BRA	GOREST		;RESET
ADRERR	MOVE	#2,EXC_ERR	;FLAG EXCEPTION CODE
	MOVE.L	2(SP),EXC_FLT	;STORE FAULT ADDRESS
	MOVE.L	10(SP),EXC_ADR	;STORE ADDRESS
	BRA	GOREST		;RESET
ILLINS	MOVE	#3,EXC_ERR	;FLAG EXCEPTION CODE
	MOVE.L	2(SP),EXC_ADR	;STORE ADDRESS
	BRA	GOREST		;RESET
ZERDIV	MOVE	#4,EXC_ERR	;FLAG EXCEPTION CODE
	MOVE.L	2(SP),EXC_ADR	;STORE ADDRESS
	BRA	GOREST		;RESET
CHKINS	MOVE	#5,EXC_ERR	;FLAG EXCEPTION CODE
	MOVE.L	2(SP),EXC_ADR	;STORE ADDRESS
	BRA	GOREST		;RESET
TRAP_V	MOVE	#6,EXC_ERR	;FLAG EXCEPTION CODE
	MOVE.L	2(SP),EXC_ADR	;STORE ADDRESS
	BRA	GOREST		;RESET
PRVVIL	MOVE	#7,EXC_ERR	;FLAG EXCEPTION CODE
	MOVE.L	2(SP),EXC_ADR	;STORE ADDRESS
	BRA	GOREST		;RESET
TRACE	MOVE	#8,EXC_ERR	;FLAG EXCEPTION CODE
	MOVE.L	2(SP),EXC_ADR	;STORE ADDRESS
	BRA	GOREST		;RESET
LIN101	MOVE	#9,EXC_ERR	;FLAG EXCEPTION CODE
	MOVE.L	2(SP),EXC_ADR	;STORE ADDRESS
	BRA	GOREST		;RESET
LIN111	MOVE	#10,EXC_ERR	;FLAG EXCEPTION CODE
	MOVE.L	2(SP),EXC_ADR	;STORE ADDRESS
	BRA	GOREST		;RESET
UNINIT	MOVE	#11,EXC_ERR	;FLAG EXCEPTION CODE
	MOVE.L	2(SP),EXC_ADR	;STORE ADDRESS
	BRA	GOREST		;RESET
SPRINT	MOVE	#12,EXC_ERR	;FLAG EXCEPTION CODE
	MOVE.L	2(SP),EXC_ADR	;STORE ADDRESS
	BRA	GOREST		;RESET
UNUSED	MOVE	#13,EXC_ERR	;FLAG EXCEPTION CODE
	MOVE.L	2(SP),EXC_ADR	;STORE ADDRESS
	BRA	GOREST		;RESET

VCTRAM	MOVEM.L	D0-D1/A0-A1,-(SP);SAVE REGS
	CLR.L	D0
	MOVE	22(SP),D0
	MOVE.L	#1024,A0
*	ADD.L	D0,A0
	MOVE.L	(A0,D0),A0
	JSR	(A0)
	MOVEM.L	(SP)+,D0-D1/A0-A1
	RTE

	PAGE
CONTAB	DC.L	C_RAMSTART1		;START OF SYSTEM RAM
	DC.L	C_RAMEND1		;END OF SYSTEM RAM
	DC.L	0			;NOT USED
	DC.L	C_RAMSTART2		;START OF DATA RAM
	DC.L	C_RAMEND2		;END OF DATA RAM
	DC.L	0			;NOT USED
	DC	C_NPROC			;MAX NUMBER OF PROCEDURES
	DC	0			;NOT USED
	DC	0			;NOT USED
	DC	0			;NOT USED
	DC	0			;NOT USED
	DC	C_NIOD			;MAX LOGICAL DEVICE NUMBER
	DC.L	IOJTABL			;ADDRESS OF I/O JUMP TABLE
	DC.L	C_SYS_STACKSIZE		;O/S SYSTEM STACK SIZE
	DC.L	.ROOT			;ROOT START ADDRESS
	DC	C_ROOT_HEAPSIZE		;NOT USED
	DC	C_ROOT_STACKSIZE	;ROOT USER STACK SIZE
	DC	0			;NOT USED
	DC.L	0			;NOT USED
	DC.L	0			;NOT USED
	DC.L	0			;NOT USED
	DC.L	0			;NOT USED
	DC.L	0			;NOT USED
	DC.L	0			;NOT USED
	DC.L	0			;NOT USED
	DC.L	0			;NOT USED
	DC.L	0			;NOT USED
	DC.L	0			;NOT USED
	DC.L	0			;NOT USED
        PAGE
IOJTABL
DEV0	JMP.L	NLINIT			;DEVICE INIT
	JMP.L	NLOPEN			;DEVICE OPEN
	JMP.L	NLCLOS			;DEVICE CLOSE
	JMP.L	NLREAD			;DEVICE READ
	JMP.L	NLWRIT			;DEVICE WRITE
	JMP.L	NLCNTL			;DEVICE CONTROL

DEV1	JMP.L	TPINIT			;NULL DEVICE INIT
	JMP.L	TPOPEN			;NULL DEVICE OPEN
	JMP.L	TPCLOS			;NULL DEVICE CLOSE
	JMP.L	TPREAD			;NULL DEVICE READ
	JMP.L	TPWRIT			;NULL DEVICE WRITE
	JMP.L	TPCNTL			;NULL DEVICE CONTROL

DEV2	JMP.L	CKINIT			;NULL DEVICE INIT
	JMP.L	CKOPEN			;NULL DEVICE OPEN
	JMP.L	CKCLOS			;NULL DEVICE CLOSE
	JMP.L	CKREAD			;NULL DEVICE READ
	JMP.L	CKWRIT			;NULL DEVICE WRITE
	JMP.L	CKCNTL			;NULL DEVICE CONTROL

DEV3	JMP.L	NLINIT			;DEVICE INIT
	JMP.L	NLOPEN			;DEVICE OPEN
	JMP.L	NLCLOS			;DEVICE CLOSE
	JMP.L	NLREAD			;DEVICE READ
	JMP.L	NLWRIT			;DEVICE WRITE
	JMP.L	NLCNTL			;DEVICE CONTROL

NOINIT	RTS
NOOPEN	RTS
NOCLOS	RTS
NOREAD	RTS
NOWRIT	RTS
NOCNTL	RTS

GOREST
	MOVE	#$2700,SR		;DISABLE IRQ'S & ENABLE SUPER MODE
	RESET				;RESET SYSTEM HARDWARE
*	MOVE.L	4,A0			;GET RESET VECTOR ADDRESS
*	JMP	(A0)
	JMP	BOOT			;GO TO BOOT CODE

SPVON
	ORI	#$2000,(SP)
	RTE
SPVOFF
	ANDI	#$DFFF,(SP)
	RTE
INTOFF	ORI	#$0700,(SP)
	RTE
INTON	ANDI	#$F8FF,(SP)
	RTE

LPSTOP
*	MOVE.W	#$800F,QMCR		;Turn odd QSM
*	MOVE	#$D048,TMCR		;Turn off TPU
	NOP
	DC.W	$0f800
	DC.W	$001c0
	DC.W	$02000
	NOP
*	MOVE	#$5048,TMCR		;Turn on TPU
*	MOVE.W	#$000F,QMCR		;Turn ON QSM
	RTE


	PAGE
	INCLUDE	SVCMON.S
	INCLUDE IOSVC.S
	INCLUDE	TSKSVC.S
	INCLUDE	SYSENT.S
	END

