#include <stdio.h>
#include	"sim332.h"
#include	"types.h"
#include	"rtc.h"
#include	"strcture.h"
#include "lcheapo.h"

#define IDEBASE  0x1100C0
#define IDERESET 0x110040
#define IDEBASE2 0x1100D0

/*-------------------------------------
	Additional constants
-------------------------------------*/
/*-------------------------------------
	Structures
-------------------------------------*/
struct ide_reg_struct
	{
	unsigned short IDEDATA;
	unsigned short IDEFEAT;
	unsigned short IDESCTC;
	unsigned short IDESECT;
	unsigned short IDECYLL;
	unsigned short IDECYLH;
	unsigned short IDEHEAD;
	unsigned short IDECMD ;
	};

struct ide_rreg_struct
	{
	unsigned short IDEDATA;
	unsigned short IDEERRR;
	unsigned short IDESCTC;
	unsigned short IDELBA0;
	unsigned short IDELBA1;
	unsigned short IDELBA2;
	unsigned short IDELBA3;
	unsigned short IDESTAT;
	};

struct ide_areg_struct
	{
	unsigned short space[6];
	unsigned short IDESTAT;
	unsigned short space2;
	};

/*-------------------------------------
	Variable declarations
-------------------------------------*/
	static volatile struct ide_reg_struct	*ideptr;
	static volatile struct ide_areg_struct	*ideaptr;

	static unsigned short tst_buf[512];

	extern time_rt timer;

	extern long msec_delay;

/***************************************************************************/
unsigned short swapshort(unsigned short in)
	{
	return ((in<<8) | ((in&0xff00)>>8));
	}

#define MAX_WAIT 1000

/***************************************************************************/
int ident_drive(unsigned short *bytes,
	unsigned short *heads,
	unsigned short *sectors,
	unsigned short *tracks)
	{
	unsigned short	i, j;
	unsigned char	sn[ 20+2 ];
	unsigned char	fw_rev[ 8+2 ];
	unsigned char	model[ 40+2 ];
	unsigned short	*s_ptr;

	ideptr->IDECMD = 0xEC00;				/* Send Identify command */
	if (wait_busy()) return 1;
//	wait_ready();
//	i = ideptr->IDESTAT;

	s_ptr = &tst_buf[0];
	for (i=0; i<256; i++)
		{
		*s_ptr++ = swapshort(ideptr->IDEDATA);
		}

	*tracks = tst_buf[1];		/* 1 = number of cylinders */

	*heads = tst_buf[3];			/* 3 = number of heads */

	*bytes = 512;

	*sectors = tst_buf[6];

	disk_size = tst_buf[61];
	disk_size = disk_size<<16;
	disk_size += tst_buf[60];

	s_ptr = (unsigned short *)&sn[0];
	for (i = 10; i < 20; i++)				/* 10-19 = serial number (right-justified) */
		{
    	*s_ptr = tst_buf[i];
		if (*s_ptr != 0x2020)				/* overwrite leading spaces */
			s_ptr++;
		}
	*s_ptr = 0;

	s_ptr = (unsigned short *)&fw_rev[0];
	for (i = 23; i < 27; i++)				/* 23-26 = firmware rev */
		{
    	*s_ptr = tst_buf[i];
		if (*s_ptr != 0x2020)				/* overwrite leading spaces */
			s_ptr++;
		}
	*s_ptr = 0;

	s_ptr = (unsigned short *)&model[0];
	for (i = 27; i < 47; i++)				/* 27-46 = model number */
		{
    	*s_ptr = tst_buf[i];
		if (*s_ptr != 0x2020)				/* overwrite leading spaces */
			s_ptr++;
		}
	*s_ptr = 0;

printf("Disk Size:%ld\n", disk_size);

printf("S/N  :%s\n", sn);
printf("F/W  :%s\n", fw_rev);
printf("Model:%s\n", model);

	return 1;
	}

/***************************************************************************/
void set_phys_drv_position(unsigned long physical_sector)
{
	ideptr->IDESECT = swapshort((unsigned short)(physical_sector&0xFF));
	physical_sector = physical_sector >> 8;
	ideptr->IDECYLL = swapshort((unsigned short)(physical_sector&0xFF));
	physical_sector = physical_sector >> 8;
	ideptr->IDECYLH = swapshort((unsigned short)(physical_sector&0xFF));
	physical_sector = physical_sector >> 8;
	ideptr->IDEHEAD = swapshort(0xE0 | (unsigned short)(physical_sector&0x0F));
}

/***************************************************************************/
read_block(unsigned char *buffer, unsigned long block, unsigned short num_sect)
{
	unsigned short i, j;
	unsigned short data, temp;
	register unsigned short *uptr, *sptr;

		{
		wait_busy();
		wait_ready();								/* wait for device ready */

		set_phys_drv_position(block);

		ideptr->IDESCTC = swapshort(1);
	
		ideptr->IDECMD = 0x2000;			/* Send Read Mult Sectors command */

//		TSK_RSCHD();				/* resched, give other tasks a chance */
		wait_busy();
		wait_ready();								/* wait for device ready */

		uptr = (unsigned short *) buffer;
		sptr = (unsigned short *) IDEBASE;
		j = 1 * 16;
		for (i=0; i<j; i++)
			{
//			wait_drq(i);
			*uptr++ = *sptr;
			*uptr++ = *sptr;
			*uptr++ = *sptr;
			*uptr++ = *sptr;
			*uptr++ = *sptr;
			*uptr++ = *sptr;
			*uptr++ = *sptr;
			*uptr++ = *sptr;
			*uptr++ = *sptr;
			*uptr++ = *sptr;
			*uptr++ = *sptr;
			*uptr++ = *sptr;
			*uptr++ = *sptr;
			*uptr++ = *sptr;
			*uptr++ = *sptr;
			*uptr++ = *sptr;
			}

		}
}

/***************************************************************************/
write_block(unsigned char *buffer, unsigned long block, unsigned short num_sect)
{
	unsigned short i, j;
	unsigned short *uptr, data, temp;

	wait_busy();
	wait_ready();								/* wait for device ready */

	set_phys_drv_position(block);

	ideptr->IDESCTC = swapshort(num_sect);

	ideptr->IDECMD = 0x3000;			/* Send Write Sectors command */
	wait_busy();
	wait_ready();								/* wait for device ready */

	j = num_sect * 16;
	uptr = (unsigned short *) buffer;
	for (i=0; i<j; i++)
		{
//		wait_drq(i);
		ideptr->IDEDATA = *uptr++;
		ideptr->IDEDATA = *uptr++;
		ideptr->IDEDATA = *uptr++;
		ideptr->IDEDATA = *uptr++;
		ideptr->IDEDATA = *uptr++;
		ideptr->IDEDATA = *uptr++;
		ideptr->IDEDATA = *uptr++;
		ideptr->IDEDATA = *uptr++;
		ideptr->IDEDATA = *uptr++;
		ideptr->IDEDATA = *uptr++;
		ideptr->IDEDATA = *uptr++;
		ideptr->IDEDATA = *uptr++;
		ideptr->IDEDATA = *uptr++;
		ideptr->IDEDATA = *uptr++;
		ideptr->IDEDATA = *uptr++;
		ideptr->IDEDATA = *uptr++;
		}

}

/****************************************************************************
	Purpose:	Power up the IDE devices..
   Returns:	Nothing
   Revised:
		01Jun01	---- (wan)
===========================================================================*/
eide_power_up()
	{
	unsigned short		i;
	unsigned short bytes,heads,sectors,tracks;

//	printf("Power Up Drive %d\n", drive);

	TPUSetPin(0, 1);							/* Turn drive ON */

	DelayMSecs(500);
	ideptr = (void *)(IDERESET);			/* RESET DRIVE */
	i = ideptr->IDEDATA;
//	DelayMSecs(500);

	ideptr = (void *)(IDEBASE);
	ideaptr = (void *)(IDEBASE2);			/* drive 0 */

	DelayMSecs(2400L);

	if (wait_spin()) return 1;
	if (wait_ready()) return 1;	/* wait for device ready */

	ideptr->IDEHEAD = 0xE000;

	if (wait_busy()) return 1;
	if (wait_ready()) return 1;	/* wait for device ready */

	return 0;

	}  /* end eide_power_up() */


/****************************************************************************
	Purpose:	Power down the IDE devices..
   Returns:	Nothing
   Revised:
		01Jun01	---- (wan)
===========================================================================*/
eide_power_down()
	{
	unsigned short		i;

	wait_busy();

 	ideptr->IDECMD  = 0xE000;			/* Send Standbye Immediate Command */
	DelayMSecs(1000L);
	wait_spin();
 	ideptr->IDEDATA  = 0x0000;			/* Send Standbye Immediate Command */
 	ideptr->IDEDATA  = 0x0000;			/* Send Standbye Immediate Command */
 	ideptr->IDEDATA  = 0x0000;			/* Send Standbye Immediate Command */
 	ideptr->IDEDATA  = 0x0000;			/* Send Standbye Immediate Command */
	DelayMSecs(1000L);

 	ideptr->IDEDATA  = 0x0000;			/* Send Standbye Immediate Command */
 	ideptr->IDEDATA  = 0x0000;			/* Send Standbye Immediate Command */
 	ideptr->IDEDATA  = 0x0000;			/* Send Standbye Immediate Command */
 	ideptr->IDEDATA  = 0x0000;			/* Send Standbye Immediate Command */

	TPUSetPin(0, 0);				/* Make sure drive is off */

	ideptr->IDEDATA  = 0x0000;			/* Send Standbye Immediate Command */
 	ideptr->IDEDATA  = 0x0000;			/* Send Standbye Immediate Command */
 	ideptr->IDEDATA  = 0x0000;			/* Send Standbye Immediate Command */
 	ideptr->IDEDATA  = 0x0000;			/* Send Standbye Immediate Command */

//	printf("IDE BUFFER STATE %04X\n", ideptr->IDEDATA);
	}  /* end eide_power_down() */


/****************************************************************************
	Purpose:	Wait for the IDE BUSY flag to clear.
   Returns:	Nothing
   Revised:
		19Dec01	---- (pld) use timer instead of counting loops
		01Jun01	---- (wan)
===========================================================================*/
wait_busy()
	{
	unsigned short	j, n;
	unsigned short			msec;

	n = 30000;
	while (n)
		{
		ServiceWatchdog();
		j = ((struct ide_areg_struct *)ideaptr)->IDESTAT;
		if ((j & 0x8000) == 0)				/* busy flag is clear */
			{
//			printf("Wait Busy Over at %d, %04x\n", n, j);
//			j = ((struct ide_rreg_struct *)ideptr)->IDEERRR;
//			printf("ERROR = %04x\n", j);
//			printf("STAT = %04x n:%d\n", j, n);

			return 0;
			}
		TSK_RSCHD();				/* resched, give other tasks a chance */
		n--;
		}
	printf("Wait Busy Timeout\n");
	return 1;
	}  /* end wait_busy() */


wait_spin()
	{
	unsigned short	j, n;
	unsigned short			msec;

	n = 30000;
	while (n)
//	while (1)
		{
		ServiceWatchdog();
		j = ((struct ide_areg_struct *)ideaptr)->IDESTAT;
		if ((j & 0x8000) == 0)				/* busy flag is clear */
			{
//			printf("Wait Busy Over at %d, %04x\n", n, j);
//			j = ((struct ide_rreg_struct *)ideptr)->IDEERRR;
//			printf("ERROR = %04x\n", j);
//			printf("STAT = %04x n:%d\n", j, n);

			return 0;
			}
		TSK_RSCHD();				/* resched, give other tasks a chance */
		n--;
		}
	printf("Wait Busy Timeout\n");
	return 1;
	}  /* end wait_busy() */


/****************************************************************************
	Purpose:	Wait for the IDE DRDY flag to set, indicating ready for commands.
   Returns:	Nothing
   Revised:
		24Jan02	---- (pld) initial development
===========================================================================*/
wait_ready()
	{
	unsigned short	j, n;
	unsigned short	msec;

	n = 30000;
	while (n)
//	while (1)
		{
		ServiceWatchdog();
		j = ((struct ide_areg_struct *)ideaptr)->IDESTAT;
		if (j & 0x4000)						/* DRDY is set */
			{
//			printf("Wait Ready Over at %d, %04x\n", n, j);
//			j = ((struct ide_rreg_struct *)ideptr)->IDEERRR;
//			printf("ERROR = %04x\n", j);
//			printf("STAT = %04x n:%d\n", j, n);
			return 0;
			}
		TSK_RSCHD();				/* resched, give other tasks a chance */
		n--;
		}
	printf("Wait Ready Timeout\n");
	return 1;
	}  /* end wait_ready() */

/****************************************************************************
	Purpose:	Wait for the IDE DRQ line to set, indicating a data request.
   Returns:	Nothing
   Revised:
		19Dec01	---- (pld) use timer instead of counting loops
		01Jun01	---- (wan)
===========================================================================*/
wait_drq(unsigned int index)
	{
	unsigned short	j, n;
	unsigned short			msec;

	n = 20000;
	while (n)
		{
		ServiceWatchdog();
		j = ((struct ide_areg_struct *)ideaptr)->IDESTAT;
		if (j & 0x0800)
			{
//			printf("Wait Over at %d, %04x\n", n, j);
			return;
			}
//		TSK_RSCHD();				/* resched, give other tasks a chance */
		n--;
		}
	printf("Wait DRQ TimeOut %d\n", index);
	}  /* end wait_drq() */

/****************************************************************************
	Purpose:	Wait for the DRQ line to clear.
   Returns:	Nothing
   Revised:
		01Jun01	---- (wan)
===========================================================================*/
wait_ndrq()
	{
	unsigned short i, j;

	i = 0;
	while (1)
		{
		ServiceWatchdog();
		if (i>10000)
			{
			break;
			}
		i += 1;
		j = ((struct ide_areg_struct *)ideaptr)->IDESTAT;
		if ((j&0x0800) == 0) break;
		TSK_RSCHD();				/* resched, give other tasks a chance */
		}
	}  /* end wait_ndrq() */

TPUSetPin(int chan, int value)
{
	dev_contrl(tpu_dev,0,chan,value);
}



