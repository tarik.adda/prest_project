/*******************************************************************************
modification par IE : XL
11/09/2013

/*******************************************************************************
Configuration d'adresse du memoire pour les temps: ann�e , mois, jour......


/*******************************************************************************
modification par IE : SB
07/05/2015

Adaptation du programme pour faire fonctionner le nouveau composant horloge (RTC) DP8573a, l'ancien IMC7170 �tant obsol�te.
Il fonctionne presque de la m�me mani�re sauf que son adressage est diff�rent.


*******************************************************************************/


#ifndef		__rtc_H
#define		__rtc_H

#define RTCBASE 0xB00000         // Adresse du depart, qui affiche par PINS:A0-A23//

/*#define		RTC_INT_VECTOR		Level_4_Interrupt */
#define		SECS1904_1980		(0x8ef45680)

typedef unsigned long time_t;

time_t mktime(struct tm *);
struct tm *localtime(time_t *);
strftime(char *, int, char *, struct tm *);

/****************************************************************************/
/* This part is accessable at both odd and even addresses (for MOVEP), also	*/
/* the command register is read-only and the status/mask register resets on	*/
/* a read so we can't use bit fields to access (R/M/W).						*/
/****************************************************************************/

/****************************************************************************/
/*Cette partie est accessible aux adresses paires et impaires (pour MOVEP)*/
/*le registre de commande est en lecture seule et le statut/masque registre
  intilialise par lecture. donc nous ne pouvons pas utiliser les champs de
  bits d'acc�s (R / M / W).  */
/****************************************************************************/
struct	RTC_				/* REAL TIME CLOCK (RTC)  [HARRIS ICM7170]		*/    /*les PINS A0-A23 */
	{
	    /*  Control Registers  */
    uchar		MSR;		    /* Main Status Register		                 */ /*adresse:0xB00000 */
    uchar		NA1_RTMR;		/* Not available when RS=0;
                                   Real Time Mode Register when RS=1;        */
    uchar		NA2_OMR;		/* Not available when RS=0;
                                   Output Mode Register when RS=1;           */
    uchar		PFR_ICR0;		/* Periodic Flag Register when RS=0 ;
                                   Interrupt Control Register 0 when RS=1    */
    uchar		TSCR_ICR1;		/* Time Save Control Register when RS=0 ;
                                   Interrupt Control Register 1 when RS=1    */

        /*  Counters (clock calendar)  */
	uchar		HNDR;		/* Counter-1/100 seconds [0-99]		    */               /*adresse:0xB00005 */
	uchar		SEC;		/* Counter-seconds [0-59]			    */               /*adresse:0xB00006 */
	uchar		MIN;		/* Counter-minutes [0-59]			    */               /*adresse:0xB00007 */
	uchar		HOUR;		/* Counter-hours [0-23] [1-12]		    */
	uchar		MDAY;		/* Counter-date [1-31]				    */
	uchar		MON;		/* Counter-month [1-12]				    */
	uchar		YEAR;		/* Counter-year [0-99]				    */
	uchar		RAM1;		/* Memory available			     	    */
	uchar		D0D1;		/* D0, D1 bits only                     */
	uchar		WDAY;		/* Counter-day of week [0-6]		    */

	uchar		NA3;		/* Not available                        */
	uchar		NA4;		/* Not available                        */
	uchar		NA5;		/* Not available                        */
	uchar		NA6;		/* Not available                        */

        /*  Time Compare RAM  */
	uchar		C_SEC;		/* Sec Compare RAM [0-59]				*/
	uchar		C_MIN;		/* Min Compare RAM [0-59]				*/
	uchar		C_HOUR;	    /* Hours Compare RAM [0-23] [1-12]		*/
	uchar		C_DOM;	    /* Day of Month Compare RAM [1-28/29/30/31]	*/
	uchar		C_MON;	    /* Month Compare RAM [1-12]			    */
	uchar		C_DOW;	    /* Date of Week RAM [1-7]			    */

        /*  Time Save RAM  */
	uchar		S_SEC;		/* Sec Save RAM [0-59]				    */
	uchar		S_MIN;		/* Min Save RAM [0-59]				    */
	uchar		S_HOUR;	    /* Hours Save RAM [0-23] [1-12]		    */
	uchar		S_DOM;	    /* Day of Month Save RAM [1-28/29/30/31]*/
	uchar		S_MON;	    /* Month Save RAM [1-12]			    */   /*adresse:0xB11101 */

	uchar		RAM2;		/* Memory available, activ with RS=1	*/   /*adresse:0xB11110 */
	uchar		RAM3;		/* Memory available/Test Mode Register  */   /*adresse:0xB11111 */

//		#define		M_GLOB		0x80	/* Global Interrupt Flag			*/
//		#define		M_DAY		0x40	/* Day Mask/Status					*/
//		#define		M_HOUR		0x20	/* Hour Mask/Status					*/
//		#define		M_MIN		0x10	/* Min. Mask/Status					*/
//		#define		M_SEC		0x08	/* Sec. Mask/Status					*/
//		#define		M_TNTH		0x04	/* 1/10 sec. Mask/Status			*/
//		#define		M_HNDR		0x02	/* 1/100 sec. Mask/Status			*/
//		#define		M_ALRM		0x01	/* Alarm/Compare Mask/Status		*/
//	uchar		CMD;		/* Command Register (read-only)		*/
//		#define		M_TEST		0x20	/* test = 1, normal = 0				*/
//		#define		M_REI		0x10	/* interrupts enabled = 1			*/
//		#define		M_RUN		0x08	/* stop = 0, run = 1				*/
//		#define		M_M24		0x04	/* 24 Hour = 1, 12 Hour = 0			*/
//		#define		M_XF1		0x02	/* Crystal Frequency				*/
//		#define		M_XF0		0x01	/* Crystal Frequency				*/
	};
#define		RTC				((volatile struct RTC_ *) RTCBASE)

#endif	/*	__rtc_H */


