/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "inttypes.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi3;

TIM_HandleTypeDef htim2;

/* USER CODE BEGIN PV */

//Trame de config LCHEAPO
//uint8_t aTxBuffer_test [14] =  {0x11,0x41,0x09,0x4A,0x08,0x32,0x03,0x00,0x00,0x00,0x00,0x00,0x40,0x10};
//Trame de wakeup + read continuous
uint8_t aTxBuffer_test[2] = { 0x11, 0x10 };

uint8_t aTxBuffer[1] = { 0x10 };
//Trame de lecture de registre
//uint8_t aTxBuffer_test[4] = { 0x11,0x21, 0x09,0x10 };

//Buffer de reception pour les lecture de registre
//uint8_t aRxBuffer1[2] = { 0xff, 0xff };
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI3_Init(void);
static void MX_TIM2_Init(void);
void choix_adc(int);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
	/* USER CODE BEGIN 1 */
//	  BSP_LED_Init(LED3);
	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */

	MX_SPI3_Init();
	MX_USB_DEVICE_Init();
	MX_GPIO_Init();

	//Timer pour le PWM
	//  MX_TIM2_Init();

	//Patiente jusqu'a pression du bouton bleu
	/* USER CODE BEGIN 2 */
//  	while (HAL_GPIO_ReadPin(button_GPIO_Port, button_Pin) != GPIO_PIN_RESET) {
//  		HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
//  		HAL_Delay(100);
//  	}
	//Timer pour le PWM sur PA0
	//	HAL_TIM_Base_Start_IT(&htim2);
	//HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
//Pseudo initialisaiton que j'aurai peut etre pas du mettre on pourra le faire grace a ioc
	//	HAL_GPIO_TogglePin(SEL1_GPIO_Port, SEL1_Pin);
//	HAL_GPIO_TogglePin(SEL2_GPIO_Port, SEL2_Pin);
//	HAL_GPIO_TogglePin(SEL3_GPIO_Port, SEL3_Pin);
//	HAL_GPIO_TogglePin(SYNCH_GPIO_Port, SYNCH_Pin);
//	HAL_GPIO_TogglePin(RESETADC_GPIO_Port, RESETADC_Pin);
	/* Enable the GPIO_LED Clock */

//	HAL_GPIO_WritePin(RESETADC_GPIO_Port, RESETADC_Pin, GPIO_PIN_SET);

	/* Configure the GPIO_LED pin PA5*/
//	GPIO_InitStruct.Pin = GPIO_PIN_5;
//	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//	GPIO_InitStruct.Pull = GPIO_NOPULL;
//	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
//	printf("%d \n", aTxBuffer[0]);
	//aTxBuffer[0] &= ~0x80;
	//aTxBuffer[0] = 132;
//	printf("%d \n", aTxBuffer[0]);
//	/* Wait for User push-button press before starting the Communication */
//	while (HAL_GPIO_ReadPin(button_GPIO_Port, button_Pin) != GPIO_PIN_RESET) {
//		HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
//		HAL_Delay(100);
//	}
//	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
	//
	printf("Initialisation & Transmission des wakeup/Read continous \n");

	printf("Choix adc 2 \n");
	choix_adc(2);
//	printf("Choix adc 3 \n");
//	choix_adc(3);
//	printf("Choix adc 4 \n");
//	choix_adc(4);

	HAL_SPI_Transmit(&hspi3, aTxBuffer_test, sizeof(aTxBuffer_test), 1000);
//	HAL_SPI_Transmit(&hspi3, aTxBuffer, sizeof(aTxBuffer), 1000);
//	HAL_SPI_Transmit(&hspi3, aTxBuffer1, sizeof(aTxBuffer1), 1000);
//	HAL_SPI_Receive(&hspi3, aRxBuffer1, sizeof(aRxBuffer1), 1000);
//		printf("%"PRIu8"\n", aRxBuffer1[0]);
//		printf("%"PRIu8"\n", aRxBuffer1[1]);

	while (1) {
// 		HAL_SPI_Transmit(&hspi3, aTxBuffer, sizeof(aTxBuffer), 1000);
//		HAL_SPI_Receive(&hspi3, aRxBuffer, sizeof(aRxBuffer), 1000);
//		printf("%"PRIu8"\n", aRxBuffer[0]);
//		printf("%"PRIu8"\n", aRxBuffer[1]);
//		HAL_Delay(100);
	}
//	printf("%"PRIu8"\n", aRxBuffer[0]);
//	printf("%"PRIu8"\n", aRxBuffer[1]);

	//Slave select activé
	HAL_GPIO_TogglePin(SEL1_GPIO_Port, SEL1_Pin);
//	HAL_SPI_Transmit(&hspi3, aTxBuffer, sizeof(aTxBuffer), 1000);
	//HAL_Delay(100);
	//TEST
//	HAL_SPI_Receive(&hspi3, aRxBuffer, sizeof(aRxBuffer), 1000);
	//FIN TEST

	//HAL_SPI_Transmit(&hspi2, &aTxBuffer[1], 1, 1000);
	HAL_GPIO_TogglePin(SEL1_GPIO_Port, SEL1_Pin);
	//HAL_SPI_Transmit(&hspi3, Dummy, sizeof(aTxBuffer), 50);
//	HAL_SPI_Receive(&hspi3, aRxBuffer, sizeof(aRxBuffer), 1000);
	//HAL_GPIO_TogglePin(SEL1_GPIO_Port, SEL1_Pin);
	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_10);
//	printf("%"PRIu8"\n", aRxBuffer[0]);
//	printf("%"PRIu8"\n", aRxBuffer[1]);
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1) {
		printf("test \n");
		// ITM_SendChar(65);
		//Toggle de led pour verif (LED2)

		HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
		HAL_Delay(1000);
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
		//	HAL_SPI_Transmit(&hspi3, aRxBuffer, sizeof(aRxBuffer), 50);
//		HAL_SPI_Receive(&hspi2, aRxBuffer, sizeof(aRxBuffer), 50);
//		Sign = aRxBuffer[0] & 0x80;
//		data = (aRxBuffer[2]) + (aRxBuffer[1] << 8) + (aRxBuffer[0] << 16);
//		//
//		if (Sign != 0) {
//			data = data | 0xFF000000;
//		}
//		//
////		printf("%d\n", count % 2);
//		printf("%"PRIu32"\n", data);
//		data = 0;
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };
	RCC_PeriphCLKInitTypeDef PeriphClkInit = { 0 };

	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
	RCC_OscInitStruct.MSIState = RCC_MSI_ON;
	RCC_OscInitStruct.MSICalibrationValue = 0;
	RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
	RCC_OscInitStruct.PLL.PLLM = 1;
	RCC_OscInitStruct.PLL.PLLN = 30;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK) {
		Error_Handler();
	}
	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
	PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLLSAI1;
	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_MSI;
	PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
	PeriphClkInit.PLLSAI1.PLLSAI1N = 24;
	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
		Error_Handler();
	}
	/** Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1)
			!= HAL_OK) {
		Error_Handler();
	}
}

/**
 * @brief SPI3 Initialization Function
 * @param None
 * @retval None
 */
static void MX_SPI3_Init(void) {

	/* USER CODE BEGIN SPI3_Init 0 */

	/* USER CODE END SPI3_Init 0 */

	/* USER CODE BEGIN SPI3_Init 1 */

	/* USER CODE END SPI3_Init 1 */
	/* SPI3 parameter configuration*/
	hspi3.Instance = SPI3;
	hspi3.Init.Mode = SPI_MODE_MASTER;
	hspi3.Init.Direction = SPI_DIRECTION_2LINES_RXONLY;
	hspi3.Init.DataSize = SPI_DATASIZE_16BIT;
	hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi3.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi3.Init.NSS = SPI_NSS_SOFT;
	hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
	hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi3.Init.CRCPolynomial = 7;
	hspi3.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
	hspi3.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
	if (HAL_SPI_Init(&hspi3) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN SPI3_Init 2 */

	/* USER CODE END SPI3_Init 2 */

}

/**
 * @brief TIM2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM2_Init(void) {

	/* USER CODE BEGIN TIM2_Init 0 */

	/* USER CODE END TIM2_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = { 0 };
	TIM_MasterConfigTypeDef sMasterConfig = { 0 };
	TIM_OC_InitTypeDef sConfigOC = { 0 };

	/* USER CODE BEGIN TIM2_Init 1 */

	/* USER CODE END TIM2_Init 1 */
	htim2.Instance = TIM2;
	htim2.Init.Prescaler = 1;
	htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim2.Init.Period = 1;
	htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim2) != HAL_OK) {
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK) {
		Error_Handler();
	}
	if (HAL_TIM_PWM_Init(&htim2) != HAL_OK) {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig)
			!= HAL_OK) {
		Error_Handler();
	}
	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = 1;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1)
			!= HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN TIM2_Init 2 */

	/* USER CODE END TIM2_Init 2 */
	HAL_TIM_MspPostInit(&htim2);

}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void) {
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, SEL2_Pin | SEL3_Pin | SYNCH_Pin | SEL1_Pin,
			GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(RESETADC_GPIO_Port, RESETADC_Pin, GPIO_PIN_SET);

	/*Configure GPIO pin : button_Pin */
	GPIO_InitStruct.Pin = button_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(button_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : SEL2_Pin SEL3_Pin PA5 SYNCH_Pin
	 SEL1_Pin */
	GPIO_InitStruct.Pin = SEL2_Pin | SEL3_Pin | GPIO_PIN_5 | SYNCH_Pin
			| SEL1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pins : PB10 RESETADC_Pin */
	GPIO_InitStruct.Pin = GPIO_PIN_10 | RESETADC_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : IRQADC_Pin */
	GPIO_InitStruct.Pin = IRQADC_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(IRQADC_GPIO_Port, &GPIO_InitStruct);

	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI4_IRQn);

}

/* USER CODE BEGIN 4 */

void choix_adc(int s) {
	switch (s) {
	case 1:
		break;
	case 2:
		//ADC 2
			//S0 =1
			HAL_GPIO_WritePin(SEL1_GPIO_Port, SEL1_Pin, GPIO_PIN_SET);
			//S1 =0
			HAL_GPIO_WritePin(RESETADC_GPIO_Port, RESETADC_Pin, GPIO_PIN_RESET);
		break;
	case 3:
		//ADC3
			//S0 =0
			HAL_GPIO_WritePin(SEL1_GPIO_Port, SEL1_Pin, GPIO_PIN_RESET);
			//S1 =1
			HAL_GPIO_WritePin(RESETADC_GPIO_Port, RESETADC_Pin, GPIO_PIN_SET);
		break;
	case 4:
		//ADC4
			//S0 =1
			HAL_GPIO_WritePin(SEL1_GPIO_Port, SEL1_Pin, GPIO_PIN_SET);
			//S1 =1
			HAL_GPIO_WritePin(RESETADC_GPIO_Port, RESETADC_Pin, GPIO_PIN_SET);
		break;
	default:
		break;
	}
}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
/* USER CODE BEGIN Error_Handler_Debug */
/* User can add his own implementation to report the HAL error return state */

/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
