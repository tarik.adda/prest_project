#define _GNU_SOURCE

#include <wiringPi.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#include <unistd.h> /* close */
#include <netdb.h> /* gethostbyname */

#include <pthread.h>

#include <sched.h>


/******************
Variables globales 
*******************/

//Valeur a passer aux thread
struct argument {
	int a;
	long long int ret;
};

int prio;


/************************
Prototypes des fonctions 
************************/

long long int get_my_nanotime();
void *Thread_Lect(void *pin); 


/**********
*** MAIN **
***********/

int main (int argc, char *argv[]){
	//Cpu qu'on choisit pour les calculs
	cpu_set_t cpuset;

	//pointeur des thread
	pthread_t thread_lect_pin1;
	pthread_t thread_lect_pin2;

	//Arguments des thread
	struct argument *A = (struct argument *)malloc(sizeof(struct argument));
	struct argument *B = (struct argument *)malloc(sizeof(struct argument));

	//Status des thread
	void *status1;
	void *status2;

	int num_cpu1=0;
	int num_cpu2=0;


	int pps_obs=0;

	int i =0;

	if( argc > 1) {
		while(i<(argc-1)){
			if(strcmp("-c1", argv[i+1]) == 0){
				num_cpu1 = strtol(argv[i+2],0,0);
				printf("Modif CPU : %d \n",num_cpu1);
			}

			if(strcmp("-c2", argv[i+1]) == 0){
				num_cpu2 = strtol(argv[i+2],0,0);
				printf("Modif CPU : %d \n",num_cpu2);
			}



			if(strcmp("-p", argv[i+1]) == 0){
				prio = strtol(argv[i+2],0,0);
				printf("Modif prio : %d \n",prio);
			}


			if(strcmp("-o", argv[i+1]) == 0){
				pps_obs = strtol(argv[i+2],0,0);
				printf("Modif pin pps : %d \n",prio);
			}


			i=i+2;
		}
	}

	//Initialisation, 1ere value = pin a lire 2eme retour
	A->a = 17;
	A->ret = 0;

	B->a = pps_obs;
	B->ret = 0;


        /*** Modif numero CPU ***/

	wiringPiSetupGpio();

	/* Création thread1 pour lect pin */
	if((pthread_create(&thread_lect_pin1,NULL,Thread_Lect,(void *)A))!=0){
		perror("bind fail");
		return 1;
	}


	/* Création thread1 pour lect pin */
	if((pthread_create(&thread_lect_pin2,NULL,Thread_Lect,(void *)B))!=0){
		perror("bind fail");
		return 1;
	}


        CPU_ZERO(&cpuset);
        CPU_SET(num_cpu1, &cpuset);


	if((pthread_setaffinity_np(thread_lect_pin1, sizeof(cpu_set_t), &cpuset))!=0){
		printf("pthread_setaffinity_np error\n\r");
	}

        CPU_ZERO(&cpuset);
        CPU_SET(num_cpu2, &cpuset);


	if((pthread_setaffinity_np(thread_lect_pin2, sizeof(cpu_set_t), &cpuset))!=0){
		printf("pthread_setaffinity_np error\n\r");
	}



	pthread_join(thread_lect_pin1,&status1);
	pthread_join(thread_lect_pin2,&status2);

	printf("val: %lld \n",A->ret);
	printf("val: %lld \n",B->ret);


	printf("Temps d'écart:  %lld \n",llabs(A->ret-B->ret));




/*
	int i;
	FILE *f1, *fopen();
	char test[POLL];

	wiringPiSetupGpio();


	for(i=0;i<POLL;i++){
		if(digitalRead(17)){
			printf("Ca fait 1");
			test[i]=digitalRead(17);
//			test[i]=0;
		} 
//else {
//		}
	}

	f1=fopen("test.txt","w");


	for(i=0;i<POLL;i++){
		
		fprintf(f1,"%c \n",test[i]);

	}
	fclose(f1);
	return(0);
*/
}

void * Thread_Lect(void *pin) {

	struct sched_param my_sched_param;

	//Valeur precedente pour detecter le rising edge
	int prec_val;
	//Valeur courante pour detecter le rising edge
	int cur_val;

	//Recuperation des arguments
	int test = ((struct argument*)pin)->a;



	printf("%d \n",test);
	//Initialisation lecture GPIO

	printf("Wiring reussi \n");
//	printf("%d \n",*test);

	//On set la priority maintenant


	my_sched_param.sched_priority = prio;
	sched_setscheduler(0,SCHED_RR, &my_sched_param);


	((struct argument*)pin)->ret = get_my_nanotime();

	printf("Sched reussi \n");

		//Initialisation de la lecture
		cur_val=digitalRead(test);
		//Lecture jusqu'a l'obtention d'un front montant
		do{
			prec_val=cur_val;
			cur_val=digitalRead(test);

		} while(!(cur_val==1 && prec_val==0));

//	((struct argument*)pin)->ret = get_my_nanotime();
	printf("verification temp : %lld  %d \n",(get_my_nanotime()/1000000000),test);
	pthread_exit(NULL);

}


/****************************************************************************
Procédure de calcul de temps
  retourne une unité temporelle en nanoseconde à l'instant ou elle est appelé
*****************************************************************************/

/* Recupération des temps d'execution*/
long long int get_my_nanotime()
{
    /* Structure pour récupérer le temps en sec. et nanosec. */
    struct timespec toto ;
    struct tm titi ;
    
    /* Temp. pour les calculs */
    long long int nb_sec ;
    long long int nb_nsec ;
    
    /* Récupération du temps sur l'horloge CLOCK_REALTIME */
    if (clock_gettime(CLOCK_REALTIME,&toto) != 0)
    {
      printf("if( clock_gettime(CLOCK_REALTIME,&toto) != 0)\r\n");
      exit(1);
    }
    /* Conversion de la valeur récupéré (il s'agissait d'une date) */
    gmtime_r(&(toto.tv_sec),&titi);
    /* Calcul du temps en  nanosecondes*/
    nb_sec = titi.tm_sec + 60 * (titi.tm_min + 60 * (titi.tm_hour /*+ 24 * titi.tm_mday)*/
));
    nb_nsec = nb_sec * 1000000000L + toto.tv_nsec ;
    return nb_nsec;
}
